package lorenzo.morelli.progettotesi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import lorenzo.morelli.progettotesi.utilities.Utilities;

public class ImageOptionBottomSheetDialog extends BottomSheetDialogFragment {

    private final View motherView;

    public ImageOptionBottomSheetDialog(final View motherView) {
        this.motherView = motherView;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.image_option_bottom_sheet_dialog,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final BottomSheetDialogFragment thisFragment = this;
        final Activity activity = getActivity();
        if (activity != null) {
            view.findViewById(R.id.bottomSheetDialog_optionTakePhoto).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utilities.takePhoto((AppCompatActivity) activity, motherView);
                    thisFragment.dismiss();
                }
            });

            view.findViewById(R.id.bottomSheetDialog_optionTakeGallery).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utilities.pickPhotoFromGallery((AppCompatActivity) activity, motherView);
                    thisFragment.dismiss();
                }
            });
        }
    }
}

