package lorenzo.morelli.progettotesi.HomeRecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import lorenzo.morelli.progettotesi.R;


/**
 * A ViewHolder describes an item view and the metadata about its place within the RecyclerView.
 */
public class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private ImageView imageCardView;
    private TextView flowerNameTextView;

    private OnItemListener itemListener;

    public CardViewHolder(@NonNull View itemView, OnItemListener lister) {
        super(itemView);
        imageCardView = itemView.findViewById(R.id.card_flower_imageView);
        flowerNameTextView = itemView.findViewById(R.id.card_flower_name_textView);
        itemListener = lister;

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        itemListener.onItemClick(getAdapterPosition());
    }

    public ImageView getImageCardView() {
        return imageCardView;
    }

    public TextView getFlowerNameTextView() {
        return flowerNameTextView;
    }
}
