package lorenzo.morelli.progettotesi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Objects;

import lorenzo.morelli.progettotesi.ViewModel.AddViewModel;
import lorenzo.morelli.progettotesi.utilities.Utilities;

public class MainActivity extends AppCompatActivity {

    private AddViewModel addViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            Utilities.insertFragment(this, new HomeFragment(),"HomeFragment");
        }

        addViewModel = new ViewModelProvider(this).get(AddViewModel.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_app_bar, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Utilities.REQUEST_IMAGE_CAPTURE) {
                Bundle extras = data.getExtras();
                if (extras != null) {
                    Bitmap imageBitmap = (Bitmap) extras.get("data");
                    this.addViewModel.setImageBitmpap(imageBitmap);
                    Utilities.insertFragment(this, new AddFragment(), "AddFragment");
                }
            }
            else if (requestCode == Utilities.REQUEST_IMAGE_PICK) {
                final Uri imageUri = data.getData();
                if (imageUri != null) {
                    try {
                        final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        final Bitmap imageBitmap = BitmapFactory.decodeStream(imageStream);
                        this.addViewModel.setImageBitmpap(imageBitmap);
                        Utilities.insertFragment(this, new AddFragment(), "AddFragment");
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        this.onBackPressed();
        Utilities.setToolBarArrow(this, false);

        return true;
    }
}