package lorenzo.morelli.progettotesi.PlantPollinatorBinder;

import android.os.AsyncTask;
import android.util.Pair;

import com.opencsv.CSVReader;

import java.util.ArrayList;
import java.util.List;

public class FindFlowerPollinatorAsyncTask extends AsyncTask<String, Void, List<Pair<String, String>>> {

    private static final int POLLINATOR_NAME_POSITION = 0;
    private static final int FLOWER_NAME_POSITION = 1;
    private static final int POLLINATOR_GROUP_POSITION = 2;

    public interface AsyncResponse {
        void processFinish(List<Pair<String, String>> output);
    }

    private final AsyncResponse delegate;
    private final CSVReader reader;

    public FindFlowerPollinatorAsyncTask(final CSVReader reader, final AsyncResponse delegate) {
        this.delegate = delegate;
        this.reader = reader;
    }

    @Override
    protected List<Pair<String, String>> doInBackground(String... strings) {
        return convert(strings[0]);
    }

    @Override
    protected void onPostExecute(List<Pair<String, String>> s) {
        super.onPostExecute(s);

        this.delegate.processFinish(s);
    }

    // X -> POLLINATOR , Y -> GROUP
    private List<Pair<String, String>> convert(String detectedFlower) {
        final List<Pair<String, String>> pollinatorList = new ArrayList<>();
        try {
            String[] nextLine;
            while ((nextLine = this.reader.readNext()) != null) {
                if (nextLine[FLOWER_NAME_POSITION].contains(detectedFlower)
                        && !pollinatorList.contains(new Pair<>(nextLine[POLLINATOR_NAME_POSITION],
                        nextLine[POLLINATOR_GROUP_POSITION]))) {
                    pollinatorList.add(new Pair<>(nextLine[POLLINATOR_NAME_POSITION],
                            nextLine[POLLINATOR_GROUP_POSITION]));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pollinatorList;
    }
}
