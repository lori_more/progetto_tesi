package lorenzo.morelli.progettotesi;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.palette.graphics.Palette;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import lorenzo.morelli.progettotesi.Database.CardItem;
import lorenzo.morelli.progettotesi.PollinatorRecyclerView.PollinatorCardAdapter;
import lorenzo.morelli.progettotesi.ViewModel.AddViewModel;
import lorenzo.morelli.progettotesi.ViewModel.ListViewModel;
import lorenzo.morelli.progettotesi.utilities.ArUtilities;
import lorenzo.morelli.progettotesi.utilities.Utilities;


public class DetailsFragment extends Fragment {

    private static final int HEIGHT_TO_CHANGE_APP_BAR_BACKGROUND = 330;
    private static final int TOPBAR_COLOR_NOT_INIT = 0;

    private PollinatorCardAdapter adapter;

    private int topbarColor = TOPBAR_COLOR_NOT_INIT;
    private MenuItem subMenu;
    private AddViewModel addViewModel;
    private CardItem thisCardItem = null;
    private MenuItem actionStar;
    private ListViewModel listViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.details_fragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final TextView flowerLatinNameTextView = view.findViewById(R.id.details_latinName_textView);
        final TextView flowerCommonNameTextView = view.findViewById(R.id.details_commonName_textView);
        final TextView insectsTipsTextView = view.findViewById(R.id.details_noInsectsTips_textView);
        final ImageView flowerImageView = view.findViewById(R.id.details_flower_imageView);

        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        if (activity != null) {
            final Toolbar thisToolbar = this.setupCustomTopBar(activity);
            this.setRecyclerView(activity);

            final TabLayout insectGroupTabLayout = activity.findViewById(R.id.details_insectGroup_tabLayout);
            insectGroupTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    String filter = "";
                    if(tab.getPosition() == Utilities.POLLINATOR_CODE_BEE) {
                        filter = Utilities.POLLINATOR_BEE_NAME;
                    } else if(tab.getPosition() == Utilities.POLLINATOR_CODE_BUTTERFLY) {
                        filter = Utilities.POLLINATOR_BUTTERFLY_NAME;
                    } else if(tab.getPosition() == Utilities.POLLINATOR_CODE_HOVERFLY) {
                        filter = Utilities.POLLINATOR_HOVERFLY_NAME;
                    }
                    adapter.getFilter().filter(filter);
                    if(adapter.getPollinatorItemListFiltered().isEmpty()) {
                        //visible
                        insectsTipsTextView.setVisibility(View.VISIBLE);
                    } else {
                        //invisible
                        insectsTipsTextView.setVisibility(View.INVISIBLE);
                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

            flowerImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (thisCardItem != null) {
                        listViewModel.selectOld(thisCardItem);
                        new ImageOptionBottomSheetDialog(view).show(activity.getSupportFragmentManager(),
                                ImageOptionBottomSheetDialog.class.getCanonicalName());
                    }
                }
            });

            addViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(AddViewModel.class);
            addViewModel.getBitmap().observe(getViewLifecycleOwner(), new Observer<Bitmap>() {
                @Override
                public void onChanged(Bitmap bitmap) {
                    if(bitmap != null && thisCardItem != null) {
                        flowerImageView.setImageBitmap(bitmap);
                        updateRecipeImage(activity, thisCardItem);
                    }
                }
            });

            // This change topbar icon color when topbar is not collapsed --------------------------
            final AppBarLayout appBarLayout = view.findViewById(R.id.details_appbarLayout);
            appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                @Override
                public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                    // 330 animation start and make background green
                    final int absVerticalOffset = Math.abs(verticalOffset);
                    if(absVerticalOffset > HEIGHT_TO_CHANGE_APP_BAR_BACKGROUND) {
                        thisToolbar.setTitleTextColor(Color.WHITE);
                        Objects.requireNonNull(thisToolbar.getNavigationIcon())
                                .setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                        if(subMenu != null && actionStar != null) {
                            subMenu.getIcon().setTint(Color.WHITE);
                            actionStar.getIcon().setTint(Color.WHITE);
                        }
                        final TypedValue themeColor = new TypedValue();
                        activity.getTheme().resolveAttribute(R.attr.colorPrimary, themeColor, true);
                        activity.getWindow().setStatusBarColor(themeColor.data);
                    } else {
                        thisToolbar.setTitleTextColor(topbarColor);
                        Objects.requireNonNull(thisToolbar.getNavigationIcon()).setColorFilter(topbarColor, PorterDuff.Mode.SRC_ATOP);
                        if(subMenu != null && actionStar != null) {
                            subMenu.getIcon().setTint(topbarColor);
                            actionStar.getIcon().setTint(topbarColor);
                        }
                        activity.getWindow().setStatusBarColor(Color.TRANSPARENT);
                    }
                }
            });

            listViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(ListViewModel.class);
            listViewModel.getSelected().observe(getViewLifecycleOwner(), new Observer<CardItem>() {
                @Override
                public void onChanged(CardItem cardItem) {
                    thisCardItem = cardItem;
                    if(actionStar != null) {
                        if(cardItem.isStarred()) {
                            actionStar.setIcon(R.drawable.ic_baseline_star_24);
                        }
                        else {
                            actionStar.setIcon(R.drawable.ic_baseline_star_empty_24);
                        }
                    }
                    thisToolbar.setTitle(cardItem.getFlowerLatinName());
                    final String latinNameToShow = getText(R.string.flower_latin_name) +
                            ": " + cardItem.getFlowerLatinName();
                    flowerLatinNameTextView.setText(latinNameToShow);
                    final String commonNameToShow = getText(R.string.flower_common_name) +
                            ": " + cardItem.getFlowerCommonName();
                    flowerCommonNameTextView.setText(commonNameToShow);
                    final String imagePath = cardItem.getImageResource();
                    if (imagePath.contains("ic_")) {
                        Drawable drawable = ContextCompat.getDrawable(activity, activity.getResources()
                                .getIdentifier(imagePath, "drawable",
                                        activity.getPackageName()));
                        flowerImageView.setImageDrawable(drawable);
                    } else {
                        Bitmap bitmap = Utilities.getImageBitmap(activity, Uri.parse(imagePath));
                        if (bitmap != null){
                            flowerImageView.setImageBitmap(bitmap);
                            flowerImageView.setBackgroundColor(Color.TRANSPARENT);

                            // Change topbar icon tint according to bitmap color
                            final Palette palette = Palette.from(bitmap).generate();
                            Palette.Swatch vibrant = palette.getVibrantSwatch();
                            if (vibrant != null) {
                                if(isColorDark(vibrant.getTitleTextColor())) {
                                    topbarColor = Color.WHITE;
                                } else {
                                    topbarColor = Color.BLACK;
                                }
                                thisToolbar.setTitleTextColor(topbarColor);
                                Objects.requireNonNull(thisToolbar.getNavigationIcon())
                                        .setColorFilter(topbarColor, PorterDuff.Mode.SRC_ATOP);
                                if(subMenu != null) {
                                    subMenu.getIcon().setTint(topbarColor);
                                }
                            }
                        }
                    }
                    adapter.setData(cardItem.getPollinators());
                    final TabLayout.Tab tab = insectGroupTabLayout.getTabAt(1);
                    Objects.requireNonNull(tab).select();
                }
            });

            view.findViewById(R.id.details_goToAr_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Activity activity = getActivity();
                    if (activity != null && ArUtilities.checkSystemSupport((AppCompatActivity) activity)) {
                        final Intent intent = new Intent(activity, ARActivity.class);
                        intent.putIntegerArrayListExtra(ARActivity.EXTRA_POLLINATOR_PROBABILITIES_CODE_ID,
                                getPollinatorCodeList(thisCardItem.getPollinators()));
                        activity.startActivity(intent);
                    }
                }
            });
        }
    }

    private ArrayList<Integer> getPollinatorCodeList(List<Pair<String, String>> pollinators) {
        ArrayList<Integer> resultArray = new ArrayList<>();
        for(Pair<String, String> pollinator : pollinators) {
            switch (pollinator.second) {
                case Utilities.POLLINATOR_BEE_NAME:
                    if(!resultArray.contains(Utilities.POLLINATOR_CODE_BEE)) {
                        resultArray.add(Utilities.POLLINATOR_CODE_BEE, Utilities.POLLINATOR_CODE_BEE);
                    }
                    break;
                case Utilities.POLLINATOR_BUTTERFLY_NAME:
                    if(!resultArray.contains(Utilities.POLLINATOR_CODE_BUTTERFLY)) {
                        resultArray.add(Utilities.POLLINATOR_CODE_BUTTERFLY, Utilities.POLLINATOR_CODE_BUTTERFLY);
                    }
                    break;
                case Utilities.POLLINATOR_HOVERFLY_NAME:
                    if(!resultArray.contains(Utilities.POLLINATOR_CODE_HOVERFLY)) {
                        resultArray.add(Utilities.POLLINATOR_CODE_HOVERFLY, Utilities.POLLINATOR_CODE_HOVERFLY);
                    }
                    break;
            }
        }
        return resultArray;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menu.findItem(R.id.app_bar_search).setVisible(false);
        menu.findItem(R.id.action_save).setVisible(false);
        menu.findItem(R.id.action_ar).setVisible(false);

        actionStar = menu.findItem(R.id.action_star);
        if(thisCardItem != null) {
            if(thisCardItem.isStarred()) {
                actionStar.setIcon(R.drawable.ic_baseline_star_24);
            }
            else {
                actionStar.setIcon(R.drawable.ic_baseline_star_empty_24);
            }
        }
        actionStar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                final Activity activity = getActivity();
                if (activity != null && thisCardItem != null) {
                    thisCardItem.setStarred(!thisCardItem.isStarred());
                    addViewModel.updateCardItem(thisCardItem);
                    listViewModel.select(thisCardItem);
                }
                return false;
            }
        });

        subMenu = menu.findItem(R.id.more_vert_menu);
        subMenu.setVisible(true);
        if(topbarColor != TOPBAR_COLOR_NOT_INIT) {
            subMenu.getIcon().setTint(topbarColor);
            actionStar.getIcon().setTint(topbarColor);
        }
        menu.findItem(R.id.action_delete).setOnMenuItemClickListener(item -> {
            final Activity activity = getActivity();
            if (activity != null && thisCardItem != null) {
                Utilities.setToolBarArrow((AppCompatActivity) activity, false);
                Utilities.deleteItemAndBackToHome((AppCompatActivity) activity, listViewModel, thisCardItem);
            }
            return false;
        });
        menu.findItem(R.id.action_share).setOnMenuItemClickListener(item -> {
            if(thisCardItem != null) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                final String titleText = getString(R.string.look_this_flower) + "\n";
                StringBuilder pollinatorsList = new StringBuilder(getString(R.string.attracts_these_pollinators) + ":\n");
                for(Pair<String, String> pollinator : thisCardItem.getPollinators()) {
                    pollinatorsList.append(pollinator.first)
                            .append(" (").append(pollinator.second).append(")").append("\n");
                }
                final String fullTextSharing = titleText + pollinatorsList;
                shareIntent.putExtra(Intent.EXTRA_TEXT, fullTextSharing);
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(thisCardItem.getImageResource()));
                shareIntent.setType("image/*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                if (getView() != null && getView().getContext() != null &&
                        shareIntent.resolveActivity(getView().getContext().getPackageManager()) != null) {
                    getView().getContext().startActivity(Intent.createChooser(shareIntent, null));
                }
            }

            return false;
        });
    }

    private void updateRecipeImage(Activity activity, CardItem oldCardItem) {
        try {
            Bitmap bitmap = addViewModel.getBitmap().getValue();
            String imageUriString;
            if (bitmap != null) {
                //method to save the image in the gallery of the device
                imageUriString = String.valueOf(Utilities.saveImage(bitmap, activity));
                //Toast.makeText(activity,"Image Saved", Toast.LENGTH_SHORT).show();
            } else {
                imageUriString = "ic_launcher_foreground";
            }

            final CardItem cardItem = new CardItem(imageUriString, oldCardItem.getFlowerLatinName(),
                    oldCardItem.getFlowerCommonName(), oldCardItem.getPollinators(), oldCardItem.isStarred());
            addViewModel.updateCardItem(cardItem);
            addViewModel.setImageBitmpap(null);

            //((AppCompatActivity) activity).getSupportFragmentManager().popBackStack();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Toolbar setupCustomTopBar(final AppCompatActivity activity) {
        final View baseToolbar = activity.findViewById(R.id.include);
        baseToolbar.setVisibility(View.INVISIBLE);
        final Toolbar thisToolbar = activity.findViewById(R.id.details_toolbar);
        activity.setSupportActionBar(thisToolbar);
        Utilities.setToolBarArrow(activity, true);
        return thisToolbar;
    }

    /**
     * Detect is color tends to dark or not
     * @param color
     * @return true if color tends Dark
     */
    private boolean isColorDark(int color){
        double darkness = 1-(0.299*Color.red(color) + 0.587*Color.green(color) + 0.114*Color.blue(color))/255;
        // It's a dark color

        Log.d("DetailsFragment", String.valueOf(darkness));
        return !(darkness < 0.5); // It's a light color
    }

    /**
     * Method to set the RecyclerView and the relative adapter
     *
     * @param activity the current activity
     */
    private void setRecyclerView(final Activity activity) {
        // Set up the RecyclerView
        RecyclerView recyclerView = getView().findViewById(R.id.details_pollinatorList_recyclerView);
        recyclerView.setHasFixedSize(true);
        adapter = new PollinatorCardAdapter(activity);
        recyclerView.setAdapter(adapter);
    }
}
