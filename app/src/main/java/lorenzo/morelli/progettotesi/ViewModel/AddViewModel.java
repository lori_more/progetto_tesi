package lorenzo.morelli.progettotesi.ViewModel;

import android.app.Application;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import lorenzo.morelli.progettotesi.Database.CardItem;
import lorenzo.morelli.progettotesi.Database.CardItemRepository;


public class AddViewModel extends AndroidViewModel {
    
    private final MutableLiveData<Bitmap> imageBitmpap = new MutableLiveData<>();

    private CardItemRepository repository;

    public AddViewModel(@NonNull Application application) {
        super(application);
        this.repository = new CardItemRepository(application);
    }

    public void setImageBitmpap(Bitmap bitmpap) {
        this.imageBitmpap.setValue(bitmpap);
    }

    public LiveData<Bitmap> getBitmap() {
        return this.imageBitmpap;
    }

    public void addCardItem(CardItem item){
        this.repository.addCardItem(item);
    }

    public void updateCardItem(CardItem item){
        this.repository.updateCardItem(item);
    }

    public void removeCardItem(final CardItem cardItem) {
        this.repository.removeCardItem(cardItem);
    }

    public List<CardItem> getCardItemList() {
        return this.repository.getCardItemList().getValue();
    }
}
