package lorenzo.morelli.progettotesi.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import java.util.List;

import lorenzo.morelli.progettotesi.Database.CardItem;
import lorenzo.morelli.progettotesi.Database.CardItemRepository;

public class ListViewModel extends AndroidViewModel {

    private LiveData<List<CardItem>> cardItems;
    private final MutableLiveData<CardItem> itemSelected = new MutableLiveData<>();
    private final MutableLiveData<CardItem> oldItemSelected = new MutableLiveData<>();
    private final MutableLiveData<CardItem> itemToDeleteSelected = new MutableLiveData<>();

    public ListViewModel(@NonNull Application application) {
        super(application);
        CardItemRepository repository = new CardItemRepository(application);
        cardItems = repository.getCardItemList();
    }

    public void select(CardItem cardItem) {
        itemSelected.setValue(cardItem);
    }

    public void selectOld(CardItem cardItem) {
        oldItemSelected.setValue(cardItem);
    }

    public LiveData<CardItem> getSelectedOld() {
        return oldItemSelected;
    }

    public LiveData<CardItem> getSelected() {
        return itemSelected;
    }

    public LiveData<List<CardItem>> getCardItems() {
        return cardItems;
    }

    public CardItem getCardItem(int position){
        return cardItems.getValue() == null ? null : cardItems.getValue().get(position);
    }

    public LiveData<CardItem> getSelectedItemToDelete() {
        return this.itemToDeleteSelected;
    }

    public void selectItemToDelete(CardItem itemToDelete) {
        itemToDeleteSelected.setValue(itemToDelete);
    }
}
