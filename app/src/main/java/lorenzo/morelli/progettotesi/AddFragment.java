package lorenzo.morelli.progettotesi;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.palette.graphics.Palette;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.tabs.TabLayout;
import com.opencsv.CSVReader;

import org.tensorflow.lite.support.label.Category;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import lorenzo.morelli.progettotesi.Database.CardItem;
import lorenzo.morelli.progettotesi.PlantPollinatorBinder.FindFlowerPollinatorAsyncTask;
import lorenzo.morelli.progettotesi.PollinatorRecyclerView.PollinatorCardAdapter;
import lorenzo.morelli.progettotesi.ViewModel.AddViewModel;
import lorenzo.morelli.progettotesi.ViewModel.ListViewModel;
import lorenzo.morelli.progettotesi.utilities.ArUtilities;
import lorenzo.morelli.progettotesi.utilities.FlowerDetector;
import lorenzo.morelli.progettotesi.utilities.Utilities;


public class AddFragment extends Fragment {

    private static final int HEIGHT_TO_CHANGE_APP_BAR_BACKGROUND = 330;
    private static final int TOPBAR_COLOR_NOT_INIT = 0;

    private static final String DAISY_LATIN_NAME = "Bellis perennis";
    private static final String ORCHID_LATIN_NAME = "Dendrobium tortile";
    private static final String SUNFLOWER_LATIN_NAME = "Helianthus";
    private static final String PASSION_FLOWER_LATIN_NAME = "Passiflora";
    private static final String PETUNIA_LATIN_NAME = "Petunia";
    private static final String ROSE_LATIN_NAME = "Rosa";
    private static final String DANDELION_LATIN_NAME = "Taraxacum";
    private static final String TULIP_LATIN_NAME = "Tulipa";

    private static final String CSV_BINDING_FILE_NAME = "binder/plant_pollinator_interactions.csv";

    private String latinFlowerName = "";
    private String commonTraducedName = "";

    private PollinatorCardAdapter adapter;

    private int topbarColor = TOPBAR_COLOR_NOT_INIT;
    private MenuItem subMenu;
    private MenuItem actionSave;

    private AddViewModel addViewModel;
    private ListViewModel listViewModel;
    private List<Pair<String, String>> allPollinatorList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_flower_fragment, container, false);
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final Activity activity = getActivity();
        if (activity != null) {
            final Toolbar thisToolbar = this.setupCustomTopBar((AppCompatActivity) activity);
            this.setRecyclerView(activity);

            final TextView flowerDetectedLatinNameTextView = activity.findViewById(R.id.add_flower_detectedLatinName_textView);
            final TextView flowerDetectedCommonNameTextView = activity.findViewById(R.id.add_flower_detectedCommonName_textView);
            final TextView insectsTipsTextView = activity.findViewById(R.id.addFlower_noInsectsTips_textView);

            final TabLayout insectGroupTabLayout = activity.findViewById(R.id.addFlower_insectGroup_tabLayout);
            insectGroupTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    String filter = "";
                    if(tab.getPosition() == Utilities.POLLINATOR_CODE_BEE) {
                        filter = Utilities.POLLINATOR_BEE_NAME;
                    } else if(tab.getPosition() == Utilities.POLLINATOR_CODE_BUTTERFLY) {
                        filter = Utilities.POLLINATOR_BUTTERFLY_NAME;
                    } else if(tab.getPosition() == Utilities.POLLINATOR_CODE_HOVERFLY) {
                        filter = Utilities.POLLINATOR_HOVERFLY_NAME;
                    }
                    adapter.getFilter().filter(filter);
                    if(adapter.getPollinatorItemListFiltered().isEmpty()) {
                        //visible
                        insectsTipsTextView.setVisibility(View.VISIBLE);
                    } else {
                        //invisible
                        insectsTipsTextView.setVisibility(View.INVISIBLE);
                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

            // This change topbar icon color when topbar is not collapsed --------------------------
            final ImageView imageView = view.findViewById(R.id.add_flower_imageView);
            // This change topbar icon color when topbar is not collapsed
            final AppBarLayout appBarLayout = view.findViewById(R.id.add_appbarLayout);
            appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                @Override
                public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                    // 330 animation start and make background green
                    final int absVerticalOffset = Math.abs(verticalOffset);
                    if(absVerticalOffset > HEIGHT_TO_CHANGE_APP_BAR_BACKGROUND) {
                        thisToolbar.setTitleTextColor(Color.WHITE);
                        Objects.requireNonNull(thisToolbar.getNavigationIcon())
                                .setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                        if(subMenu != null && actionSave != null) {
                            subMenu.getIcon().setTint(Color.WHITE);
                            actionSave.getIcon().setTint(Color.WHITE);
                        }
                        final TypedValue themeColor = new TypedValue();
                        activity.getTheme().resolveAttribute(R.attr.colorPrimary, themeColor, true);
                        activity.getWindow().setStatusBarColor(themeColor.data);
                    } else {
                        thisToolbar.setTitleTextColor(topbarColor);
                        Objects.requireNonNull(thisToolbar.getNavigationIcon()).setColorFilter(topbarColor, PorterDuff.Mode.SRC_ATOP);
                        if(subMenu != null && actionSave != null) {
                            subMenu.getIcon().setTint(topbarColor);
                            actionSave.getIcon().setTint(topbarColor);
                        }
                        activity.getWindow().setStatusBarColor(Color.TRANSPARENT);
                    }
                }
            });

            listViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(ListViewModel.class);
            addViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(AddViewModel.class);
            addViewModel.getBitmap().observe(getViewLifecycleOwner(), new Observer<Bitmap>() {
                @Override
                public void onChanged(Bitmap bitmap) {
                    if(bitmap != null) {
                        // Setup bitmap ------------------------------------------------------------
                        imageView.setImageBitmap(bitmap);
                        imageView.setBackgroundColor(Color.TRANSPARENT);
                        // Change topbar icon tint according to bitmap color
                        final Palette palette = Palette.from(bitmap).generate();
                        Palette.Swatch vibrant = palette.getVibrantSwatch();
                        if (vibrant != null) {
                            if(isColorDark(vibrant.getTitleTextColor())) {
                                topbarColor = Color.WHITE;
                            } else {
                                topbarColor = Color.BLACK;
                            }
                            thisToolbar.setTitleTextColor(topbarColor);
                            Objects.requireNonNull(thisToolbar.getNavigationIcon()).setColorFilter(topbarColor, PorterDuff.Mode.SRC_ATOP);
                            if(subMenu != null) {
                                subMenu.getIcon().setTint(topbarColor);
                            }
                        }
                        // Tensorflow analysis -----------------------------------------------------
                        final FlowerDetector flowerDetector = new FlowerDetector(activity);
                        final Category flowerCategory = flowerDetector.detectFlowerCategory(bitmap);
                        if (flowerCategory.getLabel().equals(FlowerDetector.NO_FLOWER_DETECTED_LABEL)) {
                            showErrorAlertDialog((AppCompatActivity) activity, view, getString(R.string.no_flower_detected_dialog));
                        } else {
                            latinFlowerName = flowerCategory.getLabel();
                            commonTraducedName = getCommonFlowerName(latinFlowerName);
                            final String latinNameToShow = getText(R.string.flower_latin_name) + ": " + latinFlowerName;
                            final String commonNameToShow = getText(R.string.flower_common_name) + ": " + commonTraducedName;
                            flowerDetectedLatinNameTextView.setText(latinNameToShow);
                            flowerDetectedCommonNameTextView.setText(commonNameToShow);
                            try {
                                final InputStreamReader csvFileString = new InputStreamReader(activity.getAssets().open(CSV_BINDING_FILE_NAME));
                                final BufferedReader bufferedReader = new BufferedReader(csvFileString);
                                new FindFlowerPollinatorAsyncTask(new CSVReader(bufferedReader), output -> {
                                    if (output != null) {
                                        allPollinatorList = output;
                                        adapter.setData(output);
                                        final TabLayout.Tab tab = insectGroupTabLayout.getTabAt(1);
                                        Objects.requireNonNull(tab).select();
                                    } else {
                                        showErrorAlertDialog((AppCompatActivity) activity, view,
                                                getString(R.string.error_while_loading_pollinators));
                                    }
                                }).execute(latinFlowerName);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menu.findItem(R.id.app_bar_search).setVisible(false);
        menu.findItem(R.id.action_delete).setVisible(false);
        menu.findItem(R.id.action_share).setVisible(false);
        menu.findItem(R.id.action_star).setVisible(false);

        actionSave = menu.findItem(R.id.action_save);
        actionSave.setVisible(true);
        actionSave.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                final Activity activity = getActivity();
                if(activity != null) {
                    if(listViewModel.getSelectedOld().getValue() != null) {
                        addViewModel.removeCardItem(listViewModel.getSelectedOld().getValue());
                        listViewModel.selectOld(null);
                        ((AppCompatActivity) activity).getSupportFragmentManager().popBackStack();
                    }
                    saveFlower((AppCompatActivity) activity, addViewModel);
                }
                return false;
            }
        });
        subMenu = menu.findItem(R.id.more_vert_menu);
        subMenu.setVisible(true);
        if(topbarColor != TOPBAR_COLOR_NOT_INIT) {
            subMenu.getIcon().setTint(topbarColor);
            actionSave.getIcon().setTint(topbarColor);
        }
        final MenuItem ar = menu.findItem(R.id.action_ar);
        ar.setVisible(true);
        ar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                final Activity activity = getActivity();
                if (activity != null && ArUtilities.checkSystemSupport((AppCompatActivity) activity)) {
                    saveFlower((AppCompatActivity) activity, addViewModel);
                    activity.startActivity(new Intent(activity, ARActivity.class));
                    final Intent intent = new Intent(activity, ARActivity.class);
                    intent.putIntegerArrayListExtra(ARActivity.EXTRA_POLLINATOR_PROBABILITIES_CODE_ID,
                            getPollinatorCodeList(allPollinatorList));
                    activity.startActivity(intent);
                }
                return false;
            }
        });
    }

    private ArrayList<Integer> getPollinatorCodeList(List<Pair<String, String>> pollinators) {
        ArrayList<Integer> resultArray = new ArrayList<>();
        for(Pair<String, String> pollinator : pollinators) {
            switch (pollinator.second) {
                case Utilities.POLLINATOR_BEE_NAME:
                    if(!resultArray.contains(Utilities.POLLINATOR_CODE_BEE)) {
                        resultArray.add(Utilities.POLLINATOR_CODE_BEE, Utilities.POLLINATOR_CODE_BEE);
                    }
                    break;
                case Utilities.POLLINATOR_BUTTERFLY_NAME:
                    if(!resultArray.contains(Utilities.POLLINATOR_CODE_BUTTERFLY)) {
                        resultArray.add(Utilities.POLLINATOR_CODE_BUTTERFLY, Utilities.POLLINATOR_CODE_BUTTERFLY);
                    }
                    break;
                case Utilities.POLLINATOR_HOVERFLY_NAME:
                    if(!resultArray.contains(Utilities.POLLINATOR_CODE_HOVERFLY)) {
                        resultArray.add(Utilities.POLLINATOR_CODE_HOVERFLY, Utilities.POLLINATOR_CODE_HOVERFLY);
                    }
                    break;
            }
        }
        return resultArray;
    }

    private String getCommonFlowerName(String latin) {
        switch (latin) {
            case DAISY_LATIN_NAME:
                return getText(R.string.daisy).toString();
            case ORCHID_LATIN_NAME:
                return getText(R.string.orchid).toString();
            case SUNFLOWER_LATIN_NAME:
                return getText(R.string.sunflower).toString();
            case PASSION_FLOWER_LATIN_NAME:
                return getText(R.string.passion_flower).toString();
            case PETUNIA_LATIN_NAME:
                return getText(R.string.petunia).toString();
            case ROSE_LATIN_NAME:
                return getText(R.string.rose).toString();
            case DANDELION_LATIN_NAME:
                return getText(R.string.dandelion).toString();
            case TULIP_LATIN_NAME:
                return getText(R.string.tulip).toString();
        }
        return "";
    }

    private Toolbar setupCustomTopBar(final AppCompatActivity activity) {
        final View baseToolbar = activity.findViewById(R.id.include);
        baseToolbar.setVisibility(View.INVISIBLE);
        final Toolbar thisToolbar = activity.findViewById(R.id.add_toolbar);
        activity.setSupportActionBar(thisToolbar);
        Utilities.setToolBarArrow(activity, true);
        return thisToolbar;
    }

    private void saveFlower(final AppCompatActivity activity, final AddViewModel addViewModel) {
        if (allPollinatorList != null) {
            try {
                Bitmap bitmap = addViewModel.getBitmap().getValue();
                String imageUriString;
                if (bitmap != null) {
                    //method to save the image in the gallery of the device
                    imageUriString = String.valueOf(Utilities.saveImage(bitmap, activity));
                    //Toast.makeText(activity,"Image Saved", Toast.LENGTH_SHORT).show();
                } else {
                    imageUriString = "ic_launcher_foreground";
                }

                addViewModel.addCardItem(new CardItem(imageUriString, latinFlowerName, commonTraducedName,
                        allPollinatorList, false));

                addViewModel.setImageBitmpap(null);

                activity.getSupportFragmentManager().popBackStack();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void showErrorAlertDialog(final AppCompatActivity activity, final View view, final String message) {
        final MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity, R.style.Theme_MyApp_Dialog_Alert);
        builder.setMessage(message);
        // Add the buttons
        builder.setPositiveButton(R.string.retake_a_picture_dialog, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // go back and...
                Utilities.setToolBarArrow(activity, false);
                activity.onBackPressed();
                // retake photo
                new ImageOptionBottomSheetDialog(view).show(activity.getSupportFragmentManager(),
                        ImageOptionBottomSheetDialog.class.getCanonicalName());
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // only go back
                Utilities.setToolBarArrow(activity, false);
                activity.onBackPressed();
            }
        });

        // prevent cancel when click outside dialog
        builder.setCancelable(false);
        // Create the AlertDialog
        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Detect is color tends to dark or not
     * @param color
     * @return true if color tends Dark
     */
    private boolean isColorDark(int color){
        double darkness = 1-(0.299*Color.red(color) + 0.587*Color.green(color) + 0.114*Color.blue(color))/255;
        // It's a dark color

        Log.d("AddFragment", String.valueOf(darkness));
        return !(darkness < 0.5); // It's a light color
    }

    /**
     * Method to set the RecyclerView and the relative adapter
     *
     * @param activity the current activity
     */
    private void setRecyclerView(final Activity activity) {
        // Set up the RecyclerView
        RecyclerView recyclerView = getView().findViewById(R.id.addFlower_pollinatorList_recyclerView);
        recyclerView.setHasFixedSize(true);
        adapter = new PollinatorCardAdapter(activity);
        recyclerView.setAdapter(adapter);
    }
}
