package lorenzo.morelli.progettotesi;

import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.ar.core.Anchor;
import com.google.ar.core.Frame;
import com.google.ar.core.Pose;
import com.google.ar.core.Session;
import com.google.ar.core.exceptions.SessionPausedException;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.Sceneform;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.Renderable;
import com.google.ar.sceneform.rendering.RenderableInstance;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.TransformableNode;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import lorenzo.morelli.progettotesi.utilities.InsectMovementEngine;
import lorenzo.morelli.progettotesi.utilities.Utilities;

public class ARActivity extends AppCompatActivity {

    public static final String EXTRA_POLLINATOR_PROBABILITIES_CODE_ID = "EXTRA_POLLINATOR_CODE_ID";

    private ArFragment arFragment;
    private Renderable beeModel;
    private Renderable butterflyModel;
    private Renderable hoverflyModel;
    private final List<Thread> insectThreadList = new ArrayList<>();

    private static final int MAX_INSECTS_SPAWNABLE = 40;
    private Thread spawnerThread;

    private List<Integer> pollinatorsProbabilities;
    private volatile boolean stopRequested = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ar);

        pollinatorsProbabilities = getIntent().getIntegerArrayListExtra(EXTRA_POLLINATOR_PROBABILITIES_CODE_ID);
        Log.d("ARActivity", String.valueOf(pollinatorsProbabilities));
        if(pollinatorsProbabilities.isEmpty()) {
            new MaterialAlertDialogBuilder(this)
                    .setMessage(R.string.no_insect_in_db)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setCancelable(false)
                    .show();
        }

        getSupportFragmentManager().addFragmentOnAttachListener((fragmentManager, fragment) -> {
            if (fragment.getId() == R.id.arFragment) {
                arFragment = (ArFragment) fragment;
            }
        });

        if (savedInstanceState == null) {
            if (Sceneform.isSupported(this)) {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.arFragment, ArFragment.class, null)
                        .commit();
            }
        }

        loadModels();
    }

    public void loadModels() {
        WeakReference<ARActivity> weakActivity = new WeakReference<>(this);
        ModelRenderable.builder()
                .setSource(this, Uri.parse("models/bee.glb"))
                .setIsFilamentGltf(true)
                .build()
                .thenAccept(model -> {
                    ARActivity activity = weakActivity.get();
                    if (activity != null) {
                        activity.beeModel = model;
                    }
                })
                .exceptionally(throwable -> {
                    Toast.makeText(this, "Unable to load model", Toast.LENGTH_LONG).show();
                    return null;
                });
        ModelRenderable.builder()
                .setSource(this, Uri.parse("models/butterfly.glb"))
                .setIsFilamentGltf(true)
                .build()
                .thenAccept(model -> {
                    ARActivity activity = weakActivity.get();
                    if (activity != null) {
                        activity.butterflyModel = model;
                    }
                })
                .exceptionally(throwable -> {
                    Toast.makeText(this, "Unable to load model", Toast.LENGTH_LONG).show();
                    return null;
                });
        ModelRenderable.builder()
                .setSource(this, Uri.parse("models/hoverfly.glb"))
                .setIsFilamentGltf(true)
                .build()
                .thenAccept(model -> {
                    ARActivity activity = weakActivity.get();
                    if (activity != null) {
                        activity.hoverflyModel = model;
                    }
                })
                .exceptionally(throwable -> {
                    Toast.makeText(this, "Unable to load model", Toast.LENGTH_LONG).show();
                    return null;
                });
    }

    @Override
    protected void onResume() {
        super.onResume();

        findViewById(R.id.fab_clear).setOnClickListener(v -> {
            if(this.spawnerThread == null) {
                this.spawnerThread = this.createInsectsSpawner();
                this.spawnerThread.start();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        stopRequested = true;
        this.finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private Thread createInsectsSpawner() {
        return new Thread(new Runnable() {
            @Override
            public void run() {
                while(!stopRequested) {
                    try {
                        spawnInsect();
                        Thread.sleep(1000);
                    } catch (InterruptedException | SessionPausedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void spawnInsect() {
        if(arFragment != null && beeModel != null && butterflyModel != null && hoverflyModel != null
                && this.insectThreadList.size() < MAX_INSECTS_SPAWNABLE) {
            // Create the Anchor.
            final Session session = arFragment.getArSceneView().getSession();
            final Frame arFrame = arFragment.getArSceneView().getArFrame();
            final Pose pose = arFrame.getCamera().getPose().compose(Pose.makeTranslation(0, 0, -3.5f)).extractTranslation();
            final Anchor anchor = session.createAnchor(pose);

            final int whichModel = this.getNextModelCode();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final AnchorNode anchorNode = new AnchorNode(anchor);
                    anchorNode.setParent(arFragment.getArSceneView().getScene());
                    // Create the transformable model and add it to the anchor.
                    final TransformableNode modelNode = new TransformableNode(arFragment.getTransformationSystem());
                    modelNode.setParent(anchorNode);

                    if(whichModel == Utilities.POLLINATOR_CODE_BEE) {
                        modelNode.setRenderable(beeModel);
                    } else if(whichModel == Utilities.POLLINATOR_CODE_BUTTERFLY) {
                        modelNode.setRenderable(butterflyModel);
                    } else if(whichModel == Utilities.POLLINATOR_CODE_HOVERFLY) {
                        modelNode.setRenderable(hoverflyModel);
                    }

                    modelNode.select();

                    final RenderableInstance modelInstance = modelNode.getRenderableInstance();
                    modelInstance.getMaterial().setInt("baseColorIndex", 0);

                    final InsectMovementEngine insectMovementEngine = new InsectMovementEngine(modelNode);
                    final Thread insectThread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                while(true) {
                                    Thread.sleep(100);
                                    insectMovementEngine.goToNextPosition();
                                }
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    insectThreadList.add(insectThread);
                    insectThread.start();
                }
            });
        }
    }

    private int getNextModelCode() {
        if(pollinatorsProbabilities.contains(Utilities.POLLINATOR_CODE_BUTTERFLY)) {
            return getRandomInt(pollinatorsProbabilities.stream().min(Integer::compare).get(),
                    pollinatorsProbabilities.stream().max(Integer::compare).get());
        } else {
            int whichModel;
            do {
                whichModel = getRandomInt(pollinatorsProbabilities.stream().min(Integer::compare).get(),
                        pollinatorsProbabilities.stream().max(Integer::compare).get());
            } while (whichModel != Utilities.POLLINATOR_CODE_BUTTERFLY);
            return whichModel;
        }
    }

    /**
     * @param min - The minimum.
     * @param max - The maximum.
     * @return A random int between these numbers (inclusive the minimum and maximum).
     */
    public static int getRandomInt(int min, int max) {
        return (int) ((Math.random() * (max + 1 - min)) + min);
    }
}
