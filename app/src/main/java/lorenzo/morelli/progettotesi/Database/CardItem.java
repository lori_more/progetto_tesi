package lorenzo.morelli.progettotesi.Database;

import android.util.Pair;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.List;

@Entity(tableName="item")
@TypeConverters({Converters.class})
public class CardItem {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "item_id")
    private int id;
    @ColumnInfo(name = "item_image")
    private final String imageResource;
    @ColumnInfo(name = "item_latin_name")
    private final String flowerLatinName;
    @ColumnInfo(name = "item_common_name")
    private final String flowerCommonName;
    @ColumnInfo(name = "item_pollinator")
    private final List<Pair<String, String>> pollinators;
    @ColumnInfo(name = "item_is_starred")
    private boolean isStarred;

    public CardItem(String imageResource, String flowerLatinName, String flowerCommonName, List<Pair<String, String>> pollinators, boolean isStarred){
        this.imageResource = imageResource;
        this.flowerLatinName = flowerLatinName;
        this.flowerCommonName = flowerCommonName;
        this.pollinators = pollinators;
        this.isStarred = isStarred;
    }

    public String getImageResource() {
        return imageResource;
    }

    public String getFlowerLatinName() {
        return flowerLatinName;
    }

    public String getFlowerCommonName() {
        return flowerCommonName;
    }

    public List<Pair<String, String>> getPollinators() {
        return pollinators;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setStarred(boolean starred) {
        this.isStarred = starred;
    }

    public boolean isStarred() {
        return isStarred;
    }
}
