package lorenzo.morelli.progettotesi.Database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;


import java.util.List;

@Dao
public interface CardItemDAO {
    // The selected on conflict strategy ignores a new CardItem
    // if it's exactly the same as one already in the list.
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void addCardItem(final CardItem cardItem);

    @Update
    void update(final CardItem cardItem);

    // The selected on conflict strategy ignores a new CardItem
    // if it's is not present in the list.
    @Delete
    void removeCardItem(final CardItem cardItem);

    @Transaction
    @Query("SELECT * from item ORDER BY item_id DESC")
    LiveData<List<CardItem>> getCardItems();
}
