package lorenzo.morelli.progettotesi.Database;

import android.util.Log;
import android.util.Pair;

import androidx.room.TypeConverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// List<Pair<POLLINATOR_NAME, POLLINATOR_GROUP>> --> "POLLINATOR_NAME;POLLINATOR_GROUP, ..."
public class Converters {
    @TypeConverter
    public static List<Pair<String, String>> fromString(String value) {
        List<Pair<String, String>> res = new ArrayList<>();
        List<String> stringList = Arrays.asList(value.split(","));
        Log.d("Converters", stringList.toString());
        for(String item : stringList) {
            final String[] itemSplit = item.split(";");
            if(itemSplit.length > 1) {
                res.add(new Pair<>(itemSplit[0], itemSplit[1]));
            }
        }
        return res;
    }

    @TypeConverter
    public static String fromListOfPair(List<Pair<String, String>> list) {
        StringBuilder res = new StringBuilder("");
        for(Pair<String, String> pair : list) {
            res.append(pair.first).append(";").append(pair.second).append(",");
        }
        return res.toString();
    }
}
