package lorenzo.morelli.progettotesi.utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import org.tensorflow.lite.support.image.TensorImage;
import org.tensorflow.lite.support.label.Category;

import java.io.IOException;
import java.util.List;

import lorenzo.morelli.progettotesi.ml.FlowerDetectorModel;

public class FlowerDetector {

    public static final float ACCEPTABLE_PROBABILITY_PERCENTAGE = (float) 0.70;
    public static final String NO_FLOWER_DETECTED_LABEL = "NoFlowerDetected";

    private final Context context;

    public FlowerDetector(final Context context) {
        this.context = context;
    }

    public Category detectFlowerCategory(final Bitmap image) {
        if(image != null) {
            try {
                final FlowerDetectorModel model = FlowerDetectorModel.newInstance(this.context);
                final TensorImage tensorImage = TensorImage.fromBitmap(image);

                // Runs model inference and gets result.
                final FlowerDetectorModel.Outputs outputs = model.process(tensorImage);
                final List<Category> allProbabilities = outputs.getProbabilityAsCategoryList();

                Category maxProbability = new Category(NO_FLOWER_DETECTED_LABEL, ACCEPTABLE_PROBABILITY_PERCENTAGE);
                for(final Category category : allProbabilities) {
                    if(maxProbability.getScore() < category.getScore()) {
                        maxProbability = category;
                    }
                }
                model.close();

                return maxProbability;
            } catch (IOException ioException) {
                ioException.printStackTrace();
                return null;
            }
        }
        return null;
    }
}
