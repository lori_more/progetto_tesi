package lorenzo.morelli.progettotesi.utilities;

import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.ux.TransformableNode;

/* Guide movement engine:
    1. select vector arrow random
    2. setup model rotation to arrow way
    3. select a random timeout to re change arrow way
    4. move model to arrow way
    5. repeat arrow choice when time is out
*/
public class InsectMovementEngine {

    private static final int MAX_RANDOM_TIMEOUT = 30;
    private static final int MIN_RANDOM_TIMEOUT = 0;
    private static final float STEP_PROGRESS = 0.1f;

    private final TransformableNode modelNode;
    private int timeOut = 0;
    private enum Arrow {X, Y, Z, BACK_X, BACK_Y, BACK_Z}
    private Arrow arrow;

    public InsectMovementEngine(final TransformableNode modelNode) {
        this.modelNode = modelNode;
        this.arrow = Arrow.X;
        // Setup model to arrow way
    }

    public void goToNextPosition() {
        if(timeOut <= 0) {
            timeOut = getRandomInt(MIN_RANDOM_TIMEOUT, MAX_RANDOM_TIMEOUT);
            // change arrow random
            this.arrow = Arrow.values()[getRandomInt(0, Arrow.values().length-1)];
        }
        timeOut--;
        final Vector3 vector3 = new Vector3(modelNode.getLocalPosition().x,
                modelNode.getLocalPosition().y, modelNode.getLocalPosition().z);

        switch (arrow) {
            case X:
                vector3.x = vector3.x + STEP_PROGRESS;
                this.modelNode.setLookDirection(new Vector3(-1f, 0f, 0f), Vector3.right());
                break;
            case BACK_X:
                vector3.x = vector3.x - STEP_PROGRESS;
                this.modelNode.setLookDirection(new Vector3(1f, 0f, 0f), Vector3.left());
                break;
            case Y:
                vector3.y = vector3.y + STEP_PROGRESS;
                this.modelNode.setLookDirection(new Vector3(0f, -1f, 0f), Vector3.up());
                break;
            case BACK_Y:
                vector3.y = vector3.y - STEP_PROGRESS;
                this.modelNode.setLookDirection(new Vector3(0f, 1f, 0f), Vector3.down());
                break;
            case Z:
                vector3.z = vector3.z + STEP_PROGRESS;
                this.modelNode.setLookDirection(new Vector3(0f, 0f, -1f), Vector3.forward());
                break;
            case BACK_Z:
                vector3.z = vector3.z - STEP_PROGRESS;
                this.modelNode.setLookDirection(new Vector3(0f, 0f, 1f), Vector3.back());
                break;
        }

        this.modelNode.setLocalPosition(vector3);
    }

    /**
     * @param min - The minimum.
     * @param max - The maximum.
     * @return A random double between these numbers (inclusive the minimum and maximum).
     */
    public static int getRandomInt(int min, int max) {
        return (int) ((Math.random() * (max + 1 - min)) + min);
    }
}
