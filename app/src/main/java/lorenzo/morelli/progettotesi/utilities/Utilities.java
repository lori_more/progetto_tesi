package lorenzo.morelli.progettotesi.utilities;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import lorenzo.morelli.progettotesi.Database.CardItem;
import lorenzo.morelli.progettotesi.HomeFragment;
import lorenzo.morelli.progettotesi.R;
import lorenzo.morelli.progettotesi.ViewModel.ListViewModel;

public class Utilities {

    public static final String POLLINATOR_BEE_NAME = "Bee";
    public static final String POLLINATOR_BUTTERFLY_NAME = "Butterfly";
    public static final String POLLINATOR_HOVERFLY_NAME = "Hoverfly";

    public static final int POLLINATOR_CODE_BEE = 0;
    public static final int POLLINATOR_CODE_BUTTERFLY = 1;
    public static final int POLLINATOR_CODE_HOVERFLY = 2;

    public static final int REQUEST_IMAGE_PICK = 0;
    public static final int REQUEST_IMAGE_CAPTURE = 1;


    public static void insertFragment(AppCompatActivity activity, Fragment fragment, String tag){

        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();

        transaction.replace(R.id.fragment_container_view, fragment, tag);

        if (!(fragment instanceof HomeFragment) /*&& !(fragment instanceof  SettingsFragment)*/){
            transaction.addToBackStack(tag);
        }

        transaction.commit();

    }

    public static void setUpToolbar(AppCompatActivity activity, Fragment fragment, String title){
        final Toolbar toolbar = activity.findViewById(R.id.topAppBar);
        toolbar.setTitle(title);
        activity.setSupportActionBar(toolbar);
        final View baseToolbar = activity.findViewById(R.id.include);
        baseToolbar.setVisibility(View.VISIBLE);

        if (!(fragment instanceof HomeFragment)){
            setToolBarArrow(activity, true);
        } else {
            setToolBarArrow(activity, false);
        }

        if (activity.getSupportActionBar() == null){
            activity.setSupportActionBar(toolbar);
        }
    }

    public static void setToolBarArrow(final AppCompatActivity activity, final boolean flag) {
        if (flag) {
            try {
                Objects.requireNonNull(activity.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
                activity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            } catch (NullPointerException ignored) {}
        } else {
            try {
                Objects.requireNonNull(activity.getSupportActionBar()).setDisplayHomeAsUpEnabled(false);
                activity.getSupportActionBar().setDisplayShowHomeEnabled(false);
            } catch (NullPointerException ignored) {}
        }

    }

    public static boolean checkCameraPermission(AppCompatActivity activity) {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(activity, new String[] {Manifest.permission.CAMERA}, 0);
            return false;
        }
        return true;
    }


    public static void takePhoto(AppCompatActivity activity, View view) {
        if(checkCameraPermission(activity)) {
            final Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // Ensure that there is a camera activity to handle the intent
            if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
                activity.startActivityForResult(takePictureIntent, Utilities.REQUEST_IMAGE_CAPTURE);
            } else {
                Snackbar.make(view, activity.getString(R.string.no_gallery_app_found), Snackbar.LENGTH_LONG).show();
            }
        }
    }

    public static void pickPhotoFromGallery(AppCompatActivity activity, View view) {
        final Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Ensure that there is a camera activity to handle the intent
        if (pickPhoto.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivityForResult(pickPhoto, Utilities.REQUEST_IMAGE_PICK);
        } else {
            Snackbar.make(view, activity.getString(R.string.no_gallery_app_found), Snackbar.LENGTH_LONG).show();
        }
    }

    /**
     * Method called to save the image taken as a file in the gallery
     * @param bitmap the image taken
     * @throws IOException if there are some issue with the creation of the image file
     * @return the Uri of the image saved
     */
    public static Uri saveImage(Bitmap bitmap, Activity activity) throws IOException {
        // Create an image file name
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ITALY).format(new Date());
        String name = "JPEG_" + timeStamp + "_.png";

        ContentResolver resolver = activity.getContentResolver();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name + ".jpg");
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg");
        Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
        OutputStream fos = resolver.openOutputStream(imageUri);

        //for the jpeg quality, it goes from 0 to 100
        //for the png, the quality is ignored
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        if (fos != null) {
            fos.close();
        }
        return imageUri;
    }

    public static Bitmap getImageBitmap(Activity activity, Uri currentPhotoUri){
        ContentResolver resolver = activity.getApplicationContext()
                .getContentResolver();
        try {
            InputStream stream = resolver.openInputStream(currentPhotoUri);
            Bitmap bitmap = BitmapFactory.decodeStream(stream);
            stream.close();
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void deleteItemAndBackToHome(AppCompatActivity activity, ListViewModel recipeListViewModel,
                                               CardItem itemToDelete){
        recipeListViewModel.selectItemToDelete(itemToDelete);

        FragmentManager fm = activity.getSupportFragmentManager();
        fm.popBackStackImmediate();
    }
}
