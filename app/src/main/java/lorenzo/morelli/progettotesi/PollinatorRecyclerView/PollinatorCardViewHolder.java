package lorenzo.morelli.progettotesi.PollinatorRecyclerView;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import lorenzo.morelli.progettotesi.R;


/**
 * A ViewHolder describes an item view and the metadata about its place within the RecyclerView.
 */
public class PollinatorCardViewHolder extends RecyclerView.ViewHolder {
    private final TextView pollinatorNameTextView;
    private final TextView pollinatorGroupTextView;

    public PollinatorCardViewHolder(@NonNull View itemView) {
        super(itemView);
        pollinatorNameTextView = itemView.findViewById(R.id.pollinatorCard_pollinatorName_textView);
        pollinatorGroupTextView = itemView.findViewById(R.id.pollinatorCard_pollinatorGroup_textView);
    }

    public TextView getPollinatorGroupTextView() {
        return pollinatorGroupTextView;
    }

    public TextView getPollinatorNameTextView() {
        return pollinatorNameTextView;
    }
}
