package lorenzo.morelli.progettotesi.PollinatorRecyclerView;

import android.app.Activity;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import lorenzo.morelli.progettotesi.Database.CardItem;
import lorenzo.morelli.progettotesi.R;
import lorenzo.morelli.progettotesi.utilities.Utilities;

/**
 * Adapter linked to the RecyclerView of the homePage, that extends a custom Adapter
 */
public class PollinatorCardAdapter extends RecyclerView.Adapter<PollinatorCardViewHolder> implements Filterable {

    //list that contains ALL the element added by the user
    private List<Pair<String, String>> pollinatorItemList = new ArrayList<>();
    //list that can be filtered
    private List<Pair<String, String>> pollinatorItemListFiltered = new ArrayList<>();

    private final Activity activity;

    public PollinatorCardAdapter(Activity activity) {
        this.activity = activity;
    }

    /**
     *
     * Called when RecyclerView needs a new RecyclerView.ViewHolder of the given type to represent an item.
     *
     * @param parent ViewGroup into which the new View will be added after it is bound to an adapter position.
     * @param viewType view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     */
    @NonNull
    @Override
    public PollinatorCardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.pollinator_card_layout,
                parent, false);
        return new PollinatorCardViewHolder(layoutView);
    }

    /**
     * Called by RecyclerView to display the data at the specified position.
     * This method should update the contents of the RecyclerView.ViewHolder.itemView to reflect
     * the item at the given position.
     *
     * @param holder ViewHolder which should be updated to represent the contents of the item at
     *               the given position in the data set.
     * @param position position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(@NonNull PollinatorCardViewHolder holder, int position) {
        Pair<String, String> currentPollinator = pollinatorItemList.get(position);

        String pollinator_translated = "";
        if(currentPollinator.second.equals(Utilities.POLLINATOR_BEE_NAME)) {
            pollinator_translated = activity.getText(R.string.bee).toString();
        } else if(currentPollinator.second.equals(Utilities.POLLINATOR_BUTTERFLY_NAME)) {
            pollinator_translated = activity.getText(R.string.butterfly).toString();
        } else if(currentPollinator.second.equals(Utilities.POLLINATOR_HOVERFLY_NAME)) {
            pollinator_translated = activity.getText(R.string.hoverfly).toString();
        }
        holder.getPollinatorNameTextView().setText(activity.getString(R.string.pollinator_name) + ": " + currentPollinator.first);
        holder.getPollinatorGroupTextView().setText(activity.getString(R.string.pollinator_group) + ": " + pollinator_translated);
    }

    /**
     * Method called when you have to filter a list (in our case the one with the trip
     * @return  filter that can be used to constrain data with a filtering pattern.
     *
     */
    @Override
    public Filter getFilter() {
        return cardFilter;
    }

    private final Filter cardFilter = new Filter() {
        /**
         * Called to filter the data according to the constraint
         * @param constraint constraint used to filtered the data
         * @return the result of the filtering
         */
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Pair<String, String>> filteredList = new ArrayList<>();

            //if you have no constraint --> return the full list
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(pollinatorItemListFiltered);
            } else {
                for (Pair<String, String> item : pollinatorItemListFiltered) {
                    if (item.second.toLowerCase().contentEquals(constraint.toString().toLowerCase())) {
                        filteredList.add(item);
                    }
                }
            }

            final FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        /**
         * Called to publish the filtering results in the user interface
         * @param constraint constraint used to filter the data
         * @param results the result of the filtering
         */
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            pollinatorItemList.clear();
            List<?> result = (List<?>) results.values;
            for (Object object : result) {
                if (object instanceof Pair) {
                    pollinatorItemList.add((Pair<String, String>) object);
                }
            }
            //warn the adapter that the dare are changed after the filtering
            notifyDataSetChanged();
        }
    };

    @Override
    public int getItemCount() {
        return pollinatorItemList.size();
    }

    public void setData(List<Pair<String, String>> list) {
        this.pollinatorItemList = new ArrayList<>(list);
        this.pollinatorItemListFiltered = new ArrayList<>(list);
        notifyDataSetChanged();
    }

    public List<Pair<String, String>> getPollinatorItemList() {
        return pollinatorItemList;
    }

    public List<Pair<String, String>> getPollinatorItemListFiltered() {
        return pollinatorItemListFiltered;
    }
}
