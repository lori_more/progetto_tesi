package lorenzo.morelli.progettotesi;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;


import java.util.List;

import lorenzo.morelli.progettotesi.Database.CardItem;
import lorenzo.morelli.progettotesi.HomeRecyclerView.CardAdapter;
import lorenzo.morelli.progettotesi.HomeRecyclerView.OnItemListener;
import lorenzo.morelli.progettotesi.ViewModel.AddViewModel;
import lorenzo.morelli.progettotesi.ViewModel.ListViewModel;
import lorenzo.morelli.progettotesi.utilities.Utilities;

public class HomeFragment extends Fragment implements OnItemListener{

    private static final String LOG = "Home-Fragment";

    private CardAdapter adapter;
    private RecyclerView recyclerView;

    private boolean starredOnly = false;
    private ListViewModel listViewModel;
    private Snackbar cardItemDeleteSnackbar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's UI should be attached to.
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.home_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        if (activity != null) {
            Utilities.setUpToolbar((AppCompatActivity) getActivity(), this, getString(R.string.your_flower_page));

            // reset topbar color
            final TypedValue typedValue = new TypedValue();
            final Resources.Theme theme = activity.getTheme();
            theme.resolveAttribute(R.attr.colorPrimaryVariant, typedValue, true);
            activity.getWindow().setStatusBarColor(typedValue.data);

            setRecyclerView(activity);
            showTipsIfNoCard(view);

            final AddViewModel addViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(AddViewModel.class);
            listViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(ListViewModel.class);
            //when the list of the items changed, the adapter gets the new list.
            listViewModel.getCardItems().observe((LifecycleOwner) activity, new Observer<List<CardItem>>() {
                @Override
                public void onChanged(List<CardItem> cardItems) {
                    final CardItem cardItemToDelete = listViewModel.getSelectedItemToDelete().getValue();
                    if(cardItemToDelete != null) {
                        cardItems.remove(cardItemToDelete);
                        listViewModel.selectItemToDelete(null);
                        final Snackbar.Callback callback = new Snackbar.Callback() {
                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                super.onDismissed(snackbar, event);
                                if (event == DISMISS_EVENT_TIMEOUT || event == DISMISS_EVENT_SWIPE
                                        || event == DISMISS_EVENT_CONSECUTIVE || event == DISMISS_EVENT_MANUAL) {
                                    addViewModel.removeCardItem(cardItemToDelete);
                                }
                            }
                        };
                        cardItemDeleteSnackbar = Snackbar.make(view, R.string.recipe_deleted_correctly, Snackbar.LENGTH_LONG);
                        cardItemDeleteSnackbar.addCallback(callback);
                        cardItemDeleteSnackbar.setAction(R.string.undo, v1 -> {
                            cardItemDeleteSnackbar.removeCallback(callback);
                            // trick for fire onChange in HomeFragment and reshow the hide Recipe
                            addViewModel.updateCardItem(cardItemToDelete);
                        });
                        cardItemDeleteSnackbar.show();
                    }
                    adapter.setData(cardItems);
                    showTipsIfNoCard(view);
                }
            });

            final FloatingActionButton floatingActionButton = view.findViewById(R.id.fab_add);
            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new ImageOptionBottomSheetDialog(view).show(activity.getSupportFragmentManager(),
                            ImageOptionBottomSheetDialog.class.getCanonicalName());
                }
            });
        } else {
            Log.e(LOG, "Activity is null");
        }
    }

    /**
     * Method to set the RecyclerView and the relative adapter
     *
     * @param activity the current activity
     */
    private void setRecyclerView(final Activity activity) {
        // Set up the RecyclerView
        recyclerView = getView().findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        final OnItemListener listener = this;
        adapter = new CardAdapter(activity, listener);
        recyclerView.setAdapter(adapter);
    }

    private void showTipsIfNoCard(final View view) {
        if(adapter.getItemCount() > 0) {
            view.findViewById(R.id.first_flower_tips).setVisibility(View.INVISIBLE);
        }
        else {
            view.findViewById(R.id.first_flower_tips).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onItemClick(int position) {
        final AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        if (appCompatActivity != null) {

            if(adapter.isFiltered()) {
                listViewModel.select(adapter.getCardItemListFiltered().get(position));
            } else {
                listViewModel.select(listViewModel.getCardItem(position));
            }
            listViewModel.selectOld(null);
            Utilities.insertFragment(appCompatActivity, new DetailsFragment(),
                    DetailsFragment.class.getSimpleName());
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menu.findItem(R.id.more_vert_menu).setVisible(false);
        menu.findItem(R.id.action_save).setVisible(false);

        final MenuItem actionStar = menu.findItem(R.id.action_star);
        if(starredOnly) {
            actionStar.setIcon(R.drawable.ic_baseline_starred_only_24);
            adapter.getFilter().filter(CardAdapter.STARRED_ONLY_STRING);
        } else {
            actionStar.setIcon(R.drawable.ic_baseline_star_empty_24);
        }
        actionStar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                final View view = getView();
                if(view != null) {
                    if(starredOnly) {
                        starredOnly = false;
                        item.setIcon(R.drawable.ic_baseline_star_empty_24);
                        adapter.getFilter().filter(null);
                        Snackbar.make(view, getString(R.string.filter_starred_only_off), BaseTransientBottomBar.LENGTH_SHORT).show();
                    } else {
                        starredOnly = true;
                        item.setIcon(R.drawable.ic_baseline_starred_only_24);
                        adapter.getFilter().filter(CardAdapter.STARRED_ONLY_STRING);
                        Snackbar.make(view, getString(R.string.filter_starred_only_on), BaseTransientBottomBar.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });

        final MenuItem item = menu.findItem(R.id.app_bar_search);
        final SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            /**
             * Called when the user submits the query. This could be due to a key press on the keyboard
             * or due to pressing a submit button.
             * @param query the query text that is to be submitted
             * @return true if the query has been handled by the listener, false to let the
             * SearchView perform the default action.
             */
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            /**
             * Called when the query text is changed by the user.
             * @param newText the new content of the query text field.
             * @return false if the SearchView should perform the default action of showing any
             * suggestions if available, true if the action was handled by the listener.
             */
            @Override
            public boolean onQueryTextChange(String newText) {
                if(starredOnly) {
                    adapter.getFilter().filter(CardAdapter.STARRED_ONLY_STRING + newText);
                } else {
                    adapter.getFilter().filter(newText);
                }
                return true;
            }
        });
    }
}
