import numpy as np

import tensorflow as tf
assert tf.__version__.startswith('2')

from tensorflow_examples.lite.model_maker.core.data_util.image_dataloader import ImageClassifierDataLoader
from tensorflow_examples.lite.model_maker.core.task import image_classifier
from tensorflow_examples.lite.model_maker.core.task.model_spec import mobilenet_v2_spec
from tensorflow_examples.lite.model_maker.core.task.model_spec import ImageModelSpec

#from tflite_model_maker import ExportFormat

import matplotlib.pyplot as plt

# for using GPU START
"""
import tensorflow as tf
try:
    tf_gpus = tf.config.list_physical_devices('GPU')
    for gpu in tf_gpus:
        tf.config.experimental.set_memory_growth(gpu, True)
except:
    pass
"""
# for using GPU END

# dataset
image_path = "./flowerDataset"

data = ImageClassifierDataLoader.from_folder(image_path)
train_data, test_data = data.split(0.9)


model = image_classifier.create(train_data)

loss, accuracy = model.evaluate(test_data)

#model.export(export_dir='./model/saved_model', export_format=ExportFormat.SAVED_MODEL)
model.export(export_dir='./model/tflite_model', with_metadata=True)
