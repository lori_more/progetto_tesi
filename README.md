# Tesi di Lorenzo Morelli

Questa è la repo del progetto di tesi in Programmazione Mobile di Lorenzo Morelli.

## Descrizione

Consiste in un app che permette il riconosciemento da una foto di 7 tipi di fiori
(rose, tulipani, girasoli, denti di leone, orchidee, petunie e ).
Oltre al riconoscimento, l'app identifica gli insetti impollinatori atratti dal fiore e ne permette
una rappresentazione in forma di realtà aumentata.

## Colab per creazione modello di Image classification

[Progetto Colab](https://bitbucket.org/lori_more/progetto_tesi/downloads/ImageClassificationCreator.ipynb)

## Realtà Aumentata basata su Sceneform SDK

[Sceneform SDK](https://github.com/ThomasGorisse/sceneform-android-sdk)

## License TODO
[MIT](https://choosealicense.com/licenses/mit/)
